# Snowflake AWS Terraform Module

Terraform module which creates a pipeline for getting data in AWS to a Snowflake table.

## Usage

When dealing with a dataset structure like this:
```json
[
  {
    "id": "d08c148e-a6fc-44a9-bc59-e7d3764677fc",
    "name": "John Doe",
    "favorite_pizza": "Hawaiian"
  },
  {
    "id": "a194650b-5f6a-4050-890a-2d70bc9938b2",
    "name": "Stephen Jobbs"
  }
]
```

You would use the module like this:
```hcl
module "snowflake_sample" {
  source = "bitbucket.org/liveviewtech/terraform-snowflake-aws.git?ref=master"

  name = "sample"

  columns = [
    {
      name     = "id"
      type     = "INT"
      nullable = false
    },
    {
      name     = "name"
      type     = "VARCHAR(64)"
      nullable = false
    },
    {
      name     = "favorite_pizza"
      type     = "VARCHAR(256)"
      nullable = true
    }
  ]

  task_schedule = "60 minute"
}
```

## [Inputs](variables.tf)

### `name`
- Type: string

This is the name of your table. This will also be used to construct the path in S3 that you will write your JSON data to. The account name must be unique for each account. It is recommended that you use the workspace to prefix your name. (i.e. when using the workspace `test`, your table would be `test-sample`.)

### `columns`
- Type: list(object)

Each column consists of three required fields:

- `name`
    - Type: string
    - This will automatically be capitalized when used in the table, so it is recommended to match the case of the keys in your json object for clarity.

- `type`
    - Type: string
    - This corresponds to a SQL type available in Snowflake. [See this article of supported types.](https://docs.snowflake.com/en/sql-reference/data-types.html)
    - A weird quick you will run into is "synonymous types." An example of this is the `TEXT` type. `TEXT` will apply just fine, but any future changes to your table will see that column instead as of type `VARCHAR(16777216)`. For whatever reason, Snowflake will transform synonymous types like `INT` to their base type (`NUMBER` in the case of `INT`) which will appear as a change in the Terraform plan. There is no harm in this, but it is a weird quirk to be aware of.
    - Case doesn't really matter here, but just to be safe make it `SCREAMING_SNAKE_CASE` like it appears in the docs.

- `nullable`
    - Type: bool
    - This will almost always be false, but Terraform objects require us to specify all fields unfortunately.
    - Setting this to true allows the field to be set to null if the key cannot be found in the JSON source object.

### `task_schedule`
  - Type: string

This is the frequency you wish for your data to be copied into Snowflake. Try to set this to a pretty infrequent rate if you can. Copies can be pretty expensive tasks if performed on small sets of data very frequently. Better to defer until there is a good set of data available as to not wake the Warehouse unnecessarily.

This can either be a cron expression of a minute duration. Note that you need to use the singular word `minute` after the numeric literal. So if you want your task to run every fifteen minutes, do `15 minute`.

## [Outputs](outputs.tf)

There are a few key outputs this module creates that you must use with your service to properly hook into Snowflake.

### `s3_uri`
- Type: string

This is the complete S3 URI you will need to write your files to. This would look like `s3://snowflake-<ACCOUNT_NUMBER>/<NAME>`

### `s3_bucket`
- Type: string

This is the name of the S3 bucket specific to your account that you will write your files to. This will typically be `snowflake-` followed by your AWS account number.

### `s3_path`
- Type: string

This is the path you will need to place your files into inside the S3 bucket. Your service will not have access to ANY other parts of the bucket. Attempting to write anywhere else in the S3 bucket will fail!

### `iam_policy`
- Type: object([aws_iam_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy))

This is the most important output available. To keep things as secure as possible, all access to the Snowflake bucket is handled case-by-case. With each service only being given write-only (not even read access!) to the specific path determined by `s3_path`. You **must** attach this to the profile/policy of your service for things to work correctly.
