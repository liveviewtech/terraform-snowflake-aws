output "s3_uri" {
  value = "s3://${data.aws_s3_bucket.snowflake.id}/${aws_s3_bucket_object.folder.id}"
}

output "s3_bucket" {
  value = data.aws_s3_bucket.snowflake.id
}

output "s3_path" {
  value = var.name
}

output "iam_policy" {
  value = aws_iam_policy.snowflake_writeonly
}
