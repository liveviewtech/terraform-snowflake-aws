variable "name" {
  type = string
}

variable "columns" {
  type = list(object({
    name     = string
    type     = string
    nullable = bool
  }))
  default = []
}

variable "primary_keys" {
  type = list(string)
  default = []
}

variable "task_schedule" {
  type    = string
  default = "15 minute"
}
