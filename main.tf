terraform {
  required_version = "~>1"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.50"
    }
    snowflake = {
      source  = "snowflake-labs/snowflake"
      version = "~>0.45"
    }
  }
}

data "aws_caller_identity" "current" {}

data "aws_ssm_parameter" "snowflake_private_key" {
  name = "/snowflake/private-key"
}

data "aws_ssm_parameter" "account_env" {
  name = "/acs/account/env"
}

locals {
  account_env = data.aws_ssm_parameter.account_env.value
}

provider "snowflake" {
  region      = "us-west-2"
  account     = "gra92703"
  username    = "AWS_${data.aws_caller_identity.current.account_id}"
  private_key = data.aws_ssm_parameter.snowflake_private_key.value
}

data "aws_s3_bucket" "snowflake" {
  bucket = "snowflake-${data.aws_caller_identity.current.account_id}"
}

resource "aws_s3_bucket_object" "folder" {
  bucket = data.aws_s3_bucket.snowflake.id
  acl    = "private"
  key    = "${var.name}/"
}

data "aws_iam_policy_document" "snowflake_writeonly" {
  statement {
    effect = "Allow"
    actions = [
      "s3:PutObject",
    ]
    resources = ["${data.aws_s3_bucket.snowflake.arn}/${var.name}/*"]
  }
}
resource "aws_iam_policy" "snowflake_writeonly" {
  name   = "${var.name}_snowflake-write-only-access"
  policy = data.aws_iam_policy_document.snowflake_writeonly.json
}

locals {
  database_name  = "AWS"
  schema_name    = "_${data.aws_caller_identity.current.account_id}"
  table_name     = replace(replace(upper(var.name), "/\\W/", "_"), "/([^A-Z0-9_])/", "")
  warehouse_name = "AWS"

  stage_location = "@${local.database_name}${local.schema_name}/${var.name}"
}

resource "snowflake_table" "this" {
  database = local.database_name
  schema   = local.schema_name
  name     = local.table_name

  dynamic "primary_key" {
    for_each = (length(var.primary_keys) == 0) ? {} : { keys = var.primary_keys }
    content {
      keys = [for k in primary_key.value : upper(k)]
    }
  }

  dynamic "column" {
    for_each = var.columns
    content {
      name     = upper(column.value["name"])
      type     = upper(column.value["type"])
      nullable = column.value["nullable"]
    }
  }
}

resource "snowflake_task" "copy" {
  database  = local.database_name
  schema    = local.schema_name
  warehouse = local.warehouse_name

  name     = "COPY_${local.table_name}"
  schedule = var.task_schedule
  enabled  = true

  sql_statement = "copy into \"${local.table_name}\" from '${local.stage_location}' file_format = (type = 'json', strip_outer_array = true) match_by_column_name = case_insensitive"
}

resource "snowflake_table_grant" "admin_access" {
  database_name = local.database_name
  schema_name   = local.schema_name
  table_name    = snowflake_table.this.name

  privilege = "SELECT"
  roles     = ["ACCOUNTADMIN"]

  enable_multiple_grants = true

  lifecycle {
    ignore_changes = [shares]
  }
}
